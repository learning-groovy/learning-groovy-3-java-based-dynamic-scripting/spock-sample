import spock.lang.Specification

class SampleTest extends Specification {
    def "empty list size is zero"() {
        expect:
        [].size() == 0
    }

    /*
    // Mocking
    def publisher = new Publisher()
    def subscriber = Mock(Subscriber)

    def "can cope with misbehaving subscribers"() {
        subscriber.receive(_) >> { throw new Exception() }

        when:
        publisher.send("event")
        publisher.send("event")

        then:
        2 * subscriber.receive("event")
    }
    */

    // Lists or Tables of Data
/*
    def "subscribers receive published events at least once"() {
        when: publisher.send(event)
        then: (1.._) * subscriber.receive(event)
        where: event << ["started", "paused", "stopped"]
    }
*/

    def "length of NASA mission names"() {
        expect:
        name.size() == length

        where:
        name      | length
        'Mercury' | 7
        'Gemini'  | 6
        'Apollo'  | 6
    }

    // Expecting Exceptions
    def stack = new Stack()

    def "peek on empty stack throws"() {
        when:
        stack.peek()
        then:
        thrown(EmptyStackException)
    }

    def "peek on empty stack throws exception with message"() {
        when:
        stack.peek()
        then:
        Exception e = thrown()
        e.toString().contains("EmptyStackException")
    }
}
